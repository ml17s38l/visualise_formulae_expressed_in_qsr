#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QGLWidget>
#include <QMainWindow>
//#include <QWidget>
#include <QtGui>
#include <QTableWidget>
#include "constraint.h"
#include "dealrelationship.h"
#include <sstream>
#include "paintingwidget.h"


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

public:
    void init();
    void insert();
    void menuBarInit();

protected:
    void paintEvent(QPaintEvent *event);

signals:
    void clicked(bool checked = false);

private slots:
    void changeStatue1D();
    void changeStatue2D();
    void changeStatue3D();
    void getLineValue();
    void getFileInput();
    void openHelp();

private:
    void clearQueue(queue<string>& q);
    void getPosition(int dimension);

public:
      PaintingWidget* threeDWidget;//paint in this widget
      QWidget* welcomeWidget;
      QWidget* surfaceWidegt;

private:
    int transTo =1;   
    int twoDLen =0;
    int twoDXLen = 0;
    int twoDYLen = 0;
    int threeDLen=0;
    int twoDNum =0;
    int oneDNum =0;
    int oneDLen = 0;

    std::string str1DName;
    //std::string str1D;
    std::string str1Dall;
    std::string str2DName;
    std::string str2DxE;
    std::string str2DxS;
    std::string str2Dyall;

    bool draw1d = false;
    bool draw2d = false;

    QMenu* menu[4];
    QAction* act[4];
    QMenuBar* menuBar;

    QGridLayout* mainLayout;
    QHBoxLayout* systemShowLayout;
    QTabWidget* mainTab;

    QHBoxLayout* labelH;
    QHBoxLayout* insertPromptLayoutH;
    QVBoxLayout* insertPromptLayoutV;
    QHBoxLayout* forTestLayout;

    QWidget* mainWidget;
    QWidget* labelWidget;
    QWidget* insertWidget;
    QWidget* forTest;

    QLabel* pLable;
    QLabel* insertPrompt[3];
    QLabel* prompt;

    QTextEdit* showStatus;

    QPushButton* buttonShow;
    QPushButton* buttonSelect;

    QTextEdit* textInput;
    QTextEdit* systemShow;
    QScrollArea *pArea;

};

#endif // MAINWINDOW_H
