#include "paintingwidget.h"
#include <QtOpenGL/QGLWidget>
#include <qmath.h>
#include <GL/glu.h>
#include<GL/glut.h>
#include <cmath>
#include <string>
#include <sstream>
#include <iostream>
#include <math.h>
#include <QTextStream>
#include <QPainter>
#include <QTimer>
using namespace std;

typedef struct materialStruct {
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat emission[4];
    GLfloat shininess;
} materialStruct;

static materialStruct wood = {
    { 0.0f, 0.0f, 0.0f, 1.0f  },
    {0.2f, 0.5f, 0.8f, 1.0f},
    { 0.0f, 0.0f, 0.0f, 1.0f },
    { 0.3f, 0.2f, 0.3f, 0.0f },
    0.0f
};

static materialStruct whiteShinyMaterials = {
    { 1.0, 1.0, 1.0, 1.0},
    { 1.0, 1.0, 1.0, 1.0},
    { 1.0, 1.0, 1.0, 1.0},
    { 0.0f, 0.0f, 0.0f, 1.0f },
    10.0
};

PaintingWidget::PaintingWidget(QWidget *parent):QGLWidget(parent)
{
    //use keyboard//1378 425
    //QPoint center = mapToGlobal(QPoint(1020/20,700/2));
    //QCursor::setPos(center);//1020 700 1250 750
    setFocusPolicy(Qt::StrongFocus);//this
    //obtain centre of the screen
    this->setFocus();
    this->setMouseTracking(true);

}

void PaintingWidget::createimage(QString obName){
    //std::cout<<"the name in block is"<<obName.toStdString()<<std::endl;
    QSize size(200,200);
    QImage image(size,QImage::Format_ARGB32);
    //fill background
    image.fill(qRgba(176,224,230,100));

    QPainter painter(&image);
    painter.setCompositionMode(QPainter::CompositionMode_DestinationOver);

    //font and color
    QPen pen = painter.pen();
    pen.setColor(Qt::red);
    QFont font = painter.font();
    font.setBold(true);
    font.setPixelSize(50);

    painter.setPen(pen);
    painter.setFont(font);

    //in the rectange center
    painter.drawText(image.rect(),Qt::AlignCenter,obName);

    //rectangel
    QPen pen2 = painter.pen();
    pen2.setColor(QColor(47,79,79));
    QFont font2 = painter.font();
    font.setBold(true);

    painter.setPen(pen2);
    painter.setFont(font2);
    painter.drawRect(0,0,200,200);

    //compress 0/100
    int n = 100;
    image.save("text.png","PNG",n);
}

void PaintingWidget::initializeGL()
{

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glEnable(GL_DEPTH_TEST);
    GLfloat color[4] ={0.3,0.3,0.3,0.0};
    glClearColor(color[0],color[1],color[2],color[3]);

    glEnable(GL_DEPTH_TEST); // comment out depth test to observe the result
    GLfloat ambientLight[] = {1.0f, 1.0f, 1.0f, 1.0f};
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight);
    glEnable(GL_TEXTURE_2D);

    glPushMatrix();
    //glLoadIdentity();
    GLfloat white_light[] = { 1.0, 1.0, 1.0, 1.0 };
    glLightfv(GL_LIGHT0, GL_DIFFUSE, white_light);
    glLightfv(GL_LIGHT0, GL_SPECULAR, white_light);

    GLfloat light_pos[] = {800,800,200, 1.};
    glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
    glLightf (GL_LIGHT0, GL_SPOT_CUTOFF,15.);

    glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL, GL_SEPARATE_SPECULAR_COLOR);
    glPopMatrix();

    //get 3d object Name
    stringstream sstrTheName;
    sstrTheName.clear();
    sstrTheName.str("");
    sstrTheName<<blockName;

    cameraControl(0);

}

void PaintingWidget::loadTexture(GLuint texture,image *img){

    glBindTexture(GL_TEXTURE_2D,texture);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_LUMINANCE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, img->Width(), img->Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, img->imageField());
}

void PaintingWidget::resizeGL(int w, int h)
{


    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glViewport(0, 0, w, h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45,1.0,0.1,1500);//1500

}

void PaintingWidget::paintGL()
{


    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    drawAll();

    //-----------------------
}

void PaintingWidget::drawAll(){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //-----------------------
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(eyeX,eyeY,eyeZ, viewX,viewY, viewZ, 0.0,1.0,0.0);
    glEnable(GL_TEXTURE_2D);


    glPushMatrix();
    materialStruct* p_front = &wood;

    glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
    glMaterialfv(GL_FRONT, GL_EMISSION,   p_front->emission);
    glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
    glPopMatrix();

    if(draw3d ==true)
    {
        glFlush();
        dealStr();
    }



    // flush to screen
    glFlush();

}

void PaintingWidget::keyPressEvent(QKeyEvent* event){
    bool ifChange = false;
    switch (event->key()){

    case Qt::Key_Up:
        eyeY += 10.0;
        break;
    case Qt::Key_Down:
        eyeY -= 10.0;
        break;
    case Qt::Key_D:
        eyeX += speed*cos(angleOfXZ*degTORad);
        eyeZ += speed*sin(angleOfXZ*degTORad);
        break;
    case Qt::Key_A:
        eyeX -= speed*cos(angleOfXZ*degTORad);
        eyeZ -= speed*sin(angleOfXZ*degTORad);
        break;
    case Qt::Key_W:
        eyeZ -= speed*cos(angleOfXZ*degTORad);
        eyeX += speed*sin(angleOfXZ*degTORad);
        break;
    case Qt::Key_S:
        eyeZ += speed*cos(angleOfXZ*degTORad);
        eyeX -= speed*sin(angleOfXZ*degTORad);
        break;
//    case Qt::Key_Q:
//        eyeX = 80;
//        eyeY = 80;
//        eyeZ = 80;
//        ifChange = true;
//        break;
     case Qt::Key_Left:
        cameraControl(-1);
        break;
     case Qt::Key_Right:
        cameraControl(1);
        break;

    }
    if(ifChange){
        viewX = eyeX;
        viewY = eyeY;
        viewZ = eyeZ;

    }else{
    viewX = eyeX + dx;
    viewY = eyeY + dy;
    viewZ = eyeZ + dz;
    }

    updateGL();

}


void PaintingWidget::cameraControl(int x)
{
    //Obtain the changes of angles in XZ plane
    GLfloat y = 0;
    angleOfXZ += x*0.05;
    angleOfXZY -= y*0.08;
    if(x==1){
        angleOfXZ += 2;
    }
    if(x==-1){
        angleOfXZ -= 2;
    }

    //Calculate the homogeneous coordinates
    dz = -viewDistance*cos(angleOfXZ*degTORad)* cos(angleOfXZY*degTORad);
    dx = viewDistance*sin(angleOfXZ*degTORad)* cos(angleOfXZY*degTORad);
    dy = viewDistance*sin(angleOfXZY*degTORad);
    //Change the direction of view
    viewX = eyeX + dx;
    viewY = eyeY + dy;
    viewZ = eyeZ + dz;
    //this->repaint();
    //update();
}

void PaintingWidget::blocktest(GLfloat x1, GLfloat x2,GLfloat y1,GLfloat y2, GLfloat z1,GLfloat z2)
{

    GLfloat normals[][5] = { {1., 0. ,0.}, {0., 0., -1.}, {0., 0., 1.}, {-1., 0., 0.},{0.,1.,0.},{0.,-1.,0.}};

    materialStruct* p_front = &whiteShinyMaterials;

    glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
    glMaterialfv(GL_FRONT, GL_EMISSION,   p_front->emission);
    glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,textures_array[0]);
    glNormal3fv(normals[0]);
    glBegin(GL_POLYGON);
    //glTexCoord2i(x,y) x0.0f left 1.0f right  y0.0f down 1.0f top
    //polygon leftdown lefttop righttop rightdown
    //forward
    glTexCoord2i(0,0);glVertex3f(x1,y1,z1);
    glTexCoord2i(0,1);glVertex3f(x1,y2,z1);
    glTexCoord2i(1,1);glVertex3f(x2,y2,z1);
    glTexCoord2i(1,0);glVertex3f(x2,y1,z1);
    glEnd();

    glNormal3fv(normals[1]);
    glBegin(GL_POLYGON);
    //right
    glTexCoord2i(0,0);glVertex3f(x2,y1,z1);
    glTexCoord2i(0,1);glVertex3f(x2,y2,z1);
    glTexCoord2i(1,1);glVertex3f(x2,y2,z2);
    glTexCoord2i(1,0);glVertex3f(x2,y1,z2);
    glEnd();

    glNormal3fv(normals[2]);
    glBegin(GL_POLYGON);
    //back
    glTexCoord2i(0,0);glVertex3f(x2,y1,z2);
    glTexCoord2i(0,1);glVertex3f(x2,y2,z2);
    glTexCoord2i(1,1);glVertex3f(x1,y2,z2);
    glTexCoord2i(1,0);glVertex3f(x1,y1,z2);
    glEnd();

    glNormal3fv(normals[3]);
    glBegin(GL_POLYGON);
    //left
    glTexCoord2i(0,0);glVertex3f(x1,y1,z2);
    glTexCoord2i(0,1);glVertex3f(x1,y2,z2);
    glTexCoord2i(1,1);glVertex3f(x1,y2,z1);
    glTexCoord2i(1,0);glVertex3f(x1,y1,z1);
    glEnd();

    glNormal3fv(normals[4]);
    glBegin(GL_POLYGON);
    //top
    glTexCoord2i(0,0);glVertex3f(x1,y2,z1);
    glTexCoord2i(0,1);glVertex3f(x1,y2,z2);
    glTexCoord2i(1,1);glVertex3f(x2,y2,z2);
    glTexCoord2i(1,0);glVertex3f(x2,y2,z1);
    glEnd();

    glNormal3fv(normals[5]);
    glBegin(GL_POLYGON);
    //down
    glTexCoord2i(0,0);glVertex3f(x1,y1,z1);
    glTexCoord2i(0,1);glVertex3f(x1,y1,z2);
    glTexCoord2i(1,1);glVertex3f(x2,y1,z2);
    glTexCoord2i(1,0);glVertex3f(x2,y1,z1);
    glEnd();



}

void PaintingWidget::dealStr(){
    int intThreeDNum = stoi(blockNum);
    int aryThreeDxS[intThreeDNum];
    int aryThreeDxE[intThreeDNum];
    int aryThreeDyS[intThreeDNum];
    int aryThreeDyE[intThreeDNum];
    int aryThreeDzS[intThreeDNum];
    int aryThreeDzE[intThreeDNum];

    //get 3d object Name
    QString aryName[intThreeDNum];
    string strName;
    vector<string> getName;
    stringstream sstrTheName;
    sstrTheName.clear();
    sstrTheName.str("");
    sstrTheName<<blockName;
    int countName =0;
    while(getline(sstrTheName,strName,':')){
        getName.push_back(strName);
        if(countName != 0)
            aryName[countName -1] =QString(QString::fromLocal8Bit(strName.c_str()));
        countName ++;
    }

    //get x and y and z in 3d
    //get x  start
    std::stringstream threeDxS;
    threeDxS.clear();
    threeDxS.str("");
    threeDxS<<threexs;
    string strThreeDxS;
    vector<string> getThreeDxS;
    int countThreeDxS =0;
    while(getline(threeDxS,strThreeDxS,':')){
        getThreeDxS.push_back(strThreeDxS);
        if(countThreeDxS != 0)
            aryThreeDxS[countThreeDxS -1] =stoi(strThreeDxS);
        countThreeDxS ++;
    }

    //get x end
    std::stringstream threeDxE;
    threeDxE.clear();
    threeDxE.str("");
    threeDxE<<threexe;
    string strThreeDxE;
    vector<string> getThreeDxE;
    int countThreeDxE =0;
    while(getline(threeDxE,strThreeDxE,':')){
        getThreeDxE.push_back(strThreeDxE);
        if(countThreeDxE != 0)
            aryThreeDxE[countThreeDxE-1] =stoi(strThreeDxE);
        countThreeDxE ++;
    }


    //get y  start
    std::stringstream threeDyS;
    threeDyS.clear();
    threeDyS.str("");
    threeDyS<<threeys;
    string strThreeDyS;
    vector<string> getThreeDyS;
    int countThreeDyS =0;
    while(getline(threeDyS,strThreeDyS,':')){
        getThreeDyS.push_back(strThreeDyS);
        if(countThreeDyS != 0)
            aryThreeDyS[countThreeDyS -1] =stoi(strThreeDyS);
        countThreeDyS ++;
    }

    //get y end
    std::stringstream threeDyE;
    threeDyE.clear();
    threeDyE.str("");
    threeDyE<<threeye;
    string strThreeDyE;
    vector<string> getThreeDyE;
    int countThreeDyE =0;
    while(getline(threeDyE,strThreeDyE,':')){
        getThreeDyE.push_back(strThreeDyE);
        if(countThreeDyE != 0)
            aryThreeDyE[countThreeDyE-1] =stoi(strThreeDyE);
        countThreeDyE ++;
    }

    //get z  start
    std::stringstream threeDzS;
    threeDzS.clear();
    threeDzS.str("");
    threeDzS<<threezs;
    string strThreeDzS;
    vector<string> getThreeDzS;
    int countThreeDzS =0;
    while(getline(threeDzS,strThreeDzS,':')){
        getThreeDzS.push_back(strThreeDzS);
        if(countThreeDzS != 0)
            aryThreeDzS[countThreeDzS -1] =stoi(strThreeDzS);
        countThreeDzS ++;
    }

    //get z end
    std::stringstream threeDzE;
    threeDzE.clear();
    threeDzE.str("");
    threeDzE<<threeze;
    string strThreeDzE;
    vector<string> getThreeDzE;
    int countThreeDzE =0;
    while(getline(threeDzE,strThreeDzE,':')){
        getThreeDzE.push_back(strThreeDzE);
        if(countThreeDzE != 0)
            aryThreeDzE[countThreeDzE-1] =stoi(strThreeDzE);
        countThreeDzE ++;
    }

    for(int i =0 ;i<intThreeDNum;i++){
        createimage(aryName[i]);
        glEnable(GL_TEXTURE_2D);
        glGenTextures(1,textures_array);
        loadTexture(textures_array[0],new image("./text.png"));
        loadTexture(textures_array[1],new image("./backGround.jpg"));
        glPushMatrix();
        //blocktest(300.0f,400.0f,200.0f,400.0f,200.0f,300.0f);
        blocktest(aryThreeDxS[i]*20,aryThreeDxE[i]*20,aryThreeDzS[i]*20,aryThreeDzE[i]*20,-aryThreeDyS[i]*20,-aryThreeDyE[i]*20);
        glPopMatrix();
        //            std::cout<<"real xs: "<<aryThreeDxS[i]<<" xe:"<<aryThreeDxE[i]<<" ys: "<<aryThreeDzS[i]<<" ye:"<<aryThreeDzE[i]<<
        //                                  " zs: "<<-aryThreeDyS[i]<<" ze:"<<-aryThreeDyE[i]<<std::endl;
    }
    backGround(0.0f,threeDXLen*20,0.0f,threeDZLen*20,0.0f,-threeDYLen*20);

}

void PaintingWidget::backGround(GLfloat x1, GLfloat x2,GLfloat y1,GLfloat y2, GLfloat z1,GLfloat z2)
{

    GLfloat normals[][5] = { {1., 0. ,0.}, {0., 0., -1.}, {0., 0., 1.}, {-1., 0., 0.},{0.,1.,0.},{0.,-1.,0.}};

    materialStruct* p_front = &whiteShinyMaterials;

    glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
    glMaterialfv(GL_FRONT, GL_EMISSION,   p_front->emission);
    glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,textures_array[1]);

    glNormal3fv(normals[1]);
    glBegin(GL_POLYGON);


    glNormal3fv(normals[1]);
    glBegin(GL_POLYGON);
    //right
    glTexCoord2i(0,0);glVertex3f(x2,y1,z1);
    glTexCoord2i(0,1);glVertex3f(x2,y2,z1);
    glTexCoord2i(1,1);glVertex3f(x2,y2,z2);
    glTexCoord2i(1,0);glVertex3f(x2,y1,z2);
    glEnd();

    glNormal3fv(normals[2]);
    glBegin(GL_POLYGON);
    //back
    glTexCoord2i(0,0);glVertex3f(x2,y1,z2);
    glTexCoord2i(0,1);glVertex3f(x2,y2,z2);
    glTexCoord2i(1,1);glVertex3f(x1,y2,z2);
    glTexCoord2i(1,0);glVertex3f(x1,y1,z2);
    glEnd();


    glNormal3fv(normals[5]);
    glBegin(GL_POLYGON);
    //down
    glTexCoord2i(0,0);glVertex3f(x1,y1,z1);
    glTexCoord2i(0,1);glVertex3f(x1,y1,z2);
    glTexCoord2i(1,1);glVertex3f(x2,y1,z2);
    glTexCoord2i(1,0);glVertex3f(x2,y1,z1);
    glEnd();
}




