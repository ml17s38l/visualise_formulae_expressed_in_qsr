#ifndef GRAPH_H
#define GRAPH_H
#include <iostream>
#include <sstream>
#include <string>
#include <queue>
#include <stack>

using namespace std;
extern bool ifRing;
extern stack<string> ringText;
struct edgeNode{
    int vtxNo;
    edgeNode *next;
};

struct relation{
    string text;
    relation *next;
};

struct vertexNode{
    int vertexLabel;
    edgeNode *first;
    relation *firstR;
    //bool visited;
    int indegree;
    int flag;//for find ring flag -1 is init   flag 0 is in topological      flag 1 is visit

};

struct graph{
    vertexNode *vertexList;
    int vertexes;
    int edges;
};

class ordergraph
{
public:
    ordergraph();

public: 
    void addEdge(graph *&aGraph, int vertexOut, int vertexIn,string reText);
    void emptyGraph(graph *&eGraph);
    void initGraph(graph *&iGraph, int edgeNumber);

    stringstream topologicalSort(graph *tGraph);
    int countIndegree(graph *iGraph, int vtxNum);

    void loopRing(graph *lGraph,int node);
//    void clear(queue<int>& q);

    //use for fins ring
    stack<int> ring;
    bool findRing = false;
    queue<int> saveFirt;
    //stack<string> ringText;



};

#endif // GRAPH_H
