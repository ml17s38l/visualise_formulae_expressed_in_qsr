#ifndef IMAGE_H
#define IMAGE_H
#include <QImage>
#include <GL/glu.h>

class image
{
public:

 image(const std::string& fn);

 ~image();

 const GLubyte* imageField() const;

 unsigned int Width()  const { return _width;}
 unsigned int Height() const {return _height;}
private:
 image(const image&);

 unsigned int _width;
 unsigned int _height;

 QImage* p_qimage;


 GLubyte* _image;
};

#endif // IMAGE_H
