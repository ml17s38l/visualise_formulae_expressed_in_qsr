#include "mainwindow.h"
#include <QMenuBar>         //菜单栏
#include <QMenu>
#include <QAction>
#include <QDebug>
#include <QToolBar>         //工具栏
#include <QPushButton>
#include <QStatusBar>       //状态栏
#include <QLabel>
#include <QTextEdit>        //文本编辑区
#include <QDockWidget>
#include <iostream>
#include <QTextStream>
#include <QAction>
#include <sstream>
#include <GL/glu.h>
#include <stdio.h>
#include <QKeyEvent>
#include <math.h>
#include <QPainter>
#include <QMouseEvent>
//#include "constrain.h"
//#include "dealrelationship.h"

using namespace std;
std::string inputValue;
std::string threexs;
std::string threexe;
std::string threeys;
std::string threeye;
std::string threezs;
std::string threeze;
std::string blockNum;
std::string blockName;
int threeDXLen;
int threeDYLen;
int threeDZLen;
bool draw3d;
bool iff;
queue<string> queueWrongText;
//MainWindow* mw = new MainWindow;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      mainWidget(new QWidget),
      mainLayout(new QGridLayout),
      mainTab(new QTabWidget),
      surfaceWidegt(new QWidget),
      welcomeWidget(new QWidget),
forTest(new QWidget){
    transTo = 1;
    pLable = new QLabel(QString::fromLocal8Bit("Hello ! Default 1D "));
    pLable->setAlignment(Qt::AlignCenter);
    pLable->setMinimumSize(pLable->sizeHint());

    this->statusBar()->setStyleSheet(QString("QStatusBar::item{border: 0px}"));
    this->statusBar()->setSizeGripEnabled(false);
    this->statusBar()->addWidget(pLable);


    this->resize(1250,750);//736 506
    this->setCentralWidget(mainWidget);

}


void MainWindow::openHelp(){

    QMessageBox::information(this,tr("Help"),tr("Input the formulae expressed in interval algebra, rectangle algebra or balock algebra\n"
                                                "Format of interval algebra formulse: regions1(object) 1: relation: region2(object2)\n"
                                                "e.g A:<:B\n"
                                                "Format of rectangle algebra formulse: regions1(object) 1(relation in x axis,relation in y axis) region2(object2)\n"
                                                "e.g A(<,>)B\n"
                                                "Format of block algebra formulse: regions1(object) 1(relation in x axis,relation in y axis, relation in z axis)region2(object2)\n"
                                                "e.g A(<,>,m)B\n"
                                                "Click dimension to select algebra"
                                                "this program can not deal inconsistent formulae"));
}


void MainWindow::init(){//set the main layout and menu
    mainWidget->setLayout(mainLayout);
    QFont prompt_font;
    prompt_font.setFamily("MS Shell D1g 2");
    prompt_font.setPixelSize(18);
    prompt = new QLabel;
    prompt->setText("Please insert the relationship");
    prompt->setFont(prompt_font);

    //prompt->setParent(welcomeWidget);
    prompt->setGeometry(150,200,320,30);

    //mainTab->setMouseTracking(true);

    //Scroll
    forTest->setFixedSize(1020,700);
    forTestLayout = new QHBoxLayout;

    pArea = new QScrollArea(forTest);
    pArea->setWidget(surfaceWidegt);
    pArea->setFixedSize(1000,680);
    //mainTab->viewPort()->installEventFilter(this);
    mainTab->setTabsClosable(true);
    mainTab->setEnabled(true);
    threeDWidget = new PaintingWidget(this);

    //pArea->setGeometry(150,0,1020,700);
    welcomeWidget->setFixedSize(1020,700);//1250 750 (556,476) 230, 514
    welcomeWidget->setAttribute(Qt::WA_PaintOutsidePaintEvent);
    surfaceWidegt->setFixedSize(1020,700);//1250 750 (556,476) 230, 514
    surfaceWidegt->setAttribute(Qt::WA_PaintOutsidePaintEvent);

    threeDWidget->setFixedSize(1020,700);//1250 750 (556,476) 230, 514
    threeDWidget->setAttribute(Qt::WA_PaintOutsidePaintEvent);

    pArea->setWidget(surfaceWidegt);
    pArea->setFixedSize(1000,680);
    forTestLayout->addWidget(pArea);
    forTest->setLayout(forTestLayout);
    mainTab->insertTab(0,welcomeWidget,tr("welcome"));
    mainTab->insertTab(1,forTest,tr("1/2d"));
    mainTab->insertTab(2,threeDWidget,tr("3d"));

    systemShow = new QTextEdit;
    systemShow->setReadOnly(false);
    systemShow->setFixedSize(1000,680);
    systemShow->setFont(QFont(tr("Consolas"), 10));
    systemShow->setText("Input the formulae expressed in interval algebra, rectangle algebra or balock algebra\n"
                        "Format of interval algebra formulse: regions1(object) 1: relation: region2(object2)\n"
                        "e.g A:<:B\n"
                        "Format of rectangle algebra formulse: regions1(object) 1(relation in x axis,relation in y axis) region2(object2)\n"
                        "e.g A(<,>)B\n"
                        "Format of block algebra formulse: regions1(object) 1(relation in x axis,relation in y axis, relation in z axis)region2(object2)\n"
                        "e.g A(<,>,m)B\n");
    systemShowLayout = new QHBoxLayout;
    systemShowLayout->addWidget(systemShow);

    welcomeWidget->setLayout(systemShowLayout);
    mainLayout->setMargin(30);
    mainLayout->addWidget(mainTab,0,1);
    mainLayout->setColumnStretch(1,3);

    menuBarInit();
    insert();
    Constrain constrain;
    constrain.initConstrain();
    //Constrain();//go to constrain to create the composition table


}

void MainWindow::paintEvent(QPaintEvent *event){
    if(draw1d == true ){
        //get 1d object Name
        surfaceWidegt->setFixedSize(2000,700);
        mainTab->setCurrentIndex(1);
        QString name[oneDNum];
        string strName;
        vector<string> getName;
        stringstream sstrTheName;
        sstrTheName.clear();
        sstrTheName.str("");
        sstrTheName<<str1DName;
        int countName =0;
        while(getline(sstrTheName,strName,':')){
            getName.push_back(strName);
            if(countName != 0)
                name[countName -1] =QString(QString::fromLocal8Bit(strName.c_str()));
            countName ++;
        }

        int newWeight = 1020;
        int newHigh = 700;
        if(oneDLen>40){
            newWeight = oneDLen * 25;
        }
        if(oneDNum>28){
            newHigh = oneDNum *25;
        }
        if(oneDLen>40 || oneDNum > 28){
            surfaceWidegt->setFixedSize(newWeight,newHigh);
        }
        int yPoint = newHigh / (oneDNum+1);

        QPainter painter(surfaceWidegt);
        int countDraw = 0;
        string linePoint;
        vector<string> getLinePoint;
        stringstream sstrTheOrder;
        sstrTheOrder.clear();
        sstrTheOrder.str("");
        sstrTheOrder<<str1Dall;
        while(getline(sstrTheOrder,linePoint,';')){
            int sStr,eStr;
            getLinePoint.push_back(linePoint);
            stringstream sstrGetLinePoint;
            sstrGetLinePoint<<linePoint;
            string everyLine;
            vector<string> getEvery;
            int count = 1;

            while(getline(sstrGetLinePoint,everyLine,':')){
                //get x start
                if(count==1){
                    getEvery.push_back(everyLine);
                    sStr = stoi(everyLine);
                    count =2;

                }else
                    if(count==2){//get s end
                        getEvery.push_back(everyLine);
                        eStr = stoi(everyLine);
                    }
            }
            //draw 1d
            //draw the linr
            painter.setPen(QColor(84,84,84));
            painter.drawLine(sStr * (newWeight/(oneDLen+1)),yPoint,eStr * (newWeight/(oneDLen+1)),yPoint);

            //draw the Name
            //QRectF(top left x,top left y,length,width)
            QRectF rect(sStr * (newWeight/(oneDLen+1)) ,yPoint,(eStr-sStr)*(newWeight/(oneDLen+1)),newHigh/(oneDNum+1));
            painter.setPen(QColor(147,112,219));
            painter.drawText(rect,Qt::AlignHCenter,name[countDraw]);

            //yPoint interval
            yPoint = yPoint + (newHigh / (oneDNum+1));

            countDraw ++;
        }
    }

    if(draw2d == true){
        std::cout<<"2d"<<std::endl;
        mainTab->setCurrentIndex(1);
        int aryTwoDx[twoDNum];
        int aryTwoDLength[twoDNum];

        stringstream twoDxS;
        stringstream twoDxE;
        twoDxS.clear();
        twoDxS.str("");
        twoDxE.clear();
        twoDxE.str("");
        twoDxS<<str2DxS;
        twoDxE<<str2DxE;
        //get 2d x start
        string strTwoDx;
        vector<string> getTwoDx;
        int countTwoDx =0;
        while(getline(twoDxS,strTwoDx,':')){
            getTwoDx.push_back(strTwoDx);
            if(countTwoDx != 0)
                aryTwoDx[countTwoDx -1] =stoi(strTwoDx);
            countTwoDx ++;
        }

        //get 2d x end
        string strTwoDLength;
        vector<string> getTwoDLength;
        int countTwoDLength =0;
        while(getline(twoDxE,strTwoDLength,':')){
            getTwoDLength.push_back(strTwoDLength);
            if(countTwoDLength != 0)
                aryTwoDLength[countTwoDLength-1] =stoi(strTwoDLength);
            countTwoDLength ++;
        }

        //get 2d object Name
        QString name[twoDNum];
        string strName;
        vector<string> getName;
        stringstream sstrTheName;
        sstrTheName.clear();
        sstrTheName.str("");
        sstrTheName<<str2DName;
        int countName =0;
        while(getline(sstrTheName,strName,':')){
            getName.push_back(strName);
            if(countName != 0)
                name[countName -1] =QString(QString::fromLocal8Bit(strName.c_str()));
            countName ++;
        }

        //paint

        int newWeight = 1020;
        int newHigh = 700;
        if(twoDXLen>40){
            newWeight = twoDXLen * 25;
        }
        if(twoDYLen>28){
            newHigh = twoDYLen *10;
        }
        if(twoDXLen>40 || twoDYLen > 28){
            surfaceWidegt->setFixedSize(newWeight,newHigh);
        }
        QPainter painter(surfaceWidegt);
        int countDraw = 0;
        string linePoint;
        vector<string> getLinePoint;
        stringstream sstrTheOrder;
        sstrTheOrder.clear();
        sstrTheOrder.str("");
        sstrTheOrder<<str2Dyall;
        while(getline(sstrTheOrder,linePoint,';')){
            int sStr,eStr;
            getLinePoint.push_back(linePoint);
            stringstream sstrGetLinePoint;
            sstrGetLinePoint<<linePoint;
            string everyLine;
            vector<string> getEvery;
            int count = 1;

            while(getline(sstrGetLinePoint,everyLine,':')){
                //get 2d  y start
                if(count==1){
                    getEvery.push_back(everyLine);
                    sStr = stoi(everyLine);
                    count =2;
                }else
                    if(count==2){//get 2d y end
                        getEvery.push_back(everyLine);
                        eStr = stoi(everyLine);
                    }
            }
            //draw rectangle
            painter.setPen(QColor(84,84,84));
            painter.drawRect(aryTwoDx[countDraw] * (newWeight/twoDXLen),newHigh-(eStr *(newHigh/(twoDYLen+1))),aryTwoDLength[countDraw]*(newWeight/twoDXLen),(eStr-sStr)*(newHigh/(twoDYLen+1)) );
            //draw Name
            QRectF rect(aryTwoDx[countDraw] * (newWeight/twoDXLen),newHigh-(eStr *(newHigh/(twoDYLen+1))),aryTwoDLength[countDraw]*(newWeight/twoDXLen),(eStr-sStr)*(newHigh/(twoDYLen+1)));
            painter.setPen(QColor(147,112,219));
            painter.drawText(rect,Qt::AlignHCenter,name[countDraw]);

            countDraw++;
        }
    }
}

void MainWindow::insert(){//layout

    insertWidget = new QWidget;

    insertWidget->setFixedSize(230,514);//1250 750

    insertPromptLayoutV = new QVBoxLayout;//hole

    insertPrompt[0] = new QLabel(this);
    insertPrompt[0]->setText(tr("Object"));
    insertPrompt[1] = new QLabel(this);
    insertPrompt[1]->setText(tr("Relation"));
    insertPrompt[2] = new QLabel(this);
    insertPrompt[2]->setText(tr("Object"));
    //-------------label
    labelH = new QHBoxLayout;
    labelWidget = new QWidget;
    labelH->addWidget(insertPrompt[0]);
    labelH->addWidget(insertPrompt[1]);
    labelH->addWidget(insertPrompt[2]);
    labelWidget->setLayout(labelH);

    insertPromptLayoutV->addWidget(labelWidget);
    //----------------insert


    textInput = new QTextEdit;
    textInput->setReadOnly(false);

    //buttonSelect = new QPushButton("Select filed",this);

    insertPromptLayoutV->addWidget(textInput);
    //insertPromptLayoutV->addWidget(buttonSelect);
    //connect(buttonSelect, SIGNAL(clicked(bool)),this,SLOT(getFileInput()));
    //---------------------------show status
    showStatus = new QTextEdit;
    showStatus->setReadOnly(true);
    showStatus->setMaximumHeight(40);

    //insertPromptLayoutV->addWidget(showStatus);
    //----------------------------button
    buttonShow = new QPushButton("trans",this);
    insertPromptLayoutV->addWidget(buttonShow);
    connect(buttonShow, SIGNAL(clicked(bool)),this,SLOT(getLineValue()));

    //--------------------------hole



    insertWidget->setLayout(insertPromptLayoutV);
    mainLayout->addWidget(insertWidget,0,0);



}

void MainWindow::changeStatue1D(){

    pLable->setText("Into 1D");
    transTo = 1;


}

void MainWindow::changeStatue2D(){
    pLable->setText("Into 2D");
    transTo = 2;

}

void MainWindow::changeStatue3D(){
    pLable->setText("Into 3D");
    transTo = 3;

}

void MainWindow::menuBarInit(){//menubar
    menu[0] = new QMenu("Dimension");

    act[0] = new QAction("1D",this);
    // act[0]->setShortcut(Qt::CTRL | Qt::Key_C );
    act[0]->setStatusTip("Translate in 1D");

    act[1] = new QAction("2D",this);
    //act[1]->setShortcut(Qt::CTRL | Qt::Key_O );
    act[1]->setStatusTip("Translate in 2D");

    act[2] = new QAction("3D",this);
    //act[1]->setShortcut(Qt::CTRL | Qt::Key_O );
    act[2]->setStatusTip("Translate in 3D");



    act[3] = new QAction("Help information",this);
    act[3]->setStatusTip("Display help information");

    connect(act[0],SIGNAL(triggered(bool)),this,SLOT(changeStatue1D()));
    connect(act[1],SIGNAL(triggered(bool)),this,SLOT(changeStatue2D()));
    connect(act[2],SIGNAL(triggered(bool)),this,SLOT(changeStatue3D()));
    connect(act[3],SIGNAL(triggered(bool)),this,SLOT(openHelp()));




    menu[0]->addAction(act[0]);
    menu[0]->addAction(act[1]);
    menu[0]->addAction(act[2]);

    menu[1] = new QMenu("Help");
    menu[1]->addAction(act[3]);

    menuBar = new QMenuBar(this);
    menuBar->addMenu(menu[0]);
    menuBar->addMenu(menu[1]);
    menuBar->setGeometry(0,0,this->width(),30);


}

// 1 =1dimension x start and x end  | 2 =2dimension x start and x end | 3=2dimension y start and y end |
//4=3dimension x start and x end | 5=3dimension y start and y end | 6=3dimension z start and z end
void MainWindow::getPosition(int dimension){
    //get the sort
    dealRelationship deal;
    //get the object name and sort
    stringstream theName,theOrder;
    int length,intObNum;

    std::stringstream twoDxS;
    std::stringstream twoDxE;

    std::stringstream threeDxS;
    std::stringstream threeDxE;
    std::stringstream threeDyS;
    std::stringstream threeDyE;
    std::stringstream threeDzS;
    std::stringstream threeDzE;


    deal.initDeal(&theName,&theOrder,&length,&intObNum);

if(queueWrongText.size() ==0){
    std::cout<<"        Name "<<theName.str()<<std::endl;
    std::cout<<"        starr and end point: "<<theOrder.str()<<std::endl;
    std::cout<<"        point of length "<<length<<std::endl;
    //get the name array
    QString name[intObNum];
    if(dimension == 1 || dimension == 2|| dimension==4){
        string strName;
        vector<string> getName;
        int countName =0;
        while(getline(theName,strName,':')){
            getName.push_back(strName);
            if(countName != 0)
                name[countName -1] =QString(QString::fromLocal8Bit(strName.c_str()));
            countName ++;
        }
    }

    //get 1d theOrder and theName
    if(dimension == 1){
        str1Dall = theOrder.str();
        oneDNum = intObNum;
        str1DName = theName.str();
        oneDLen = length + 1;
    }

    if(dimension == 2){
        twoDXLen = length +1;
    }
    //get 2d y theOrder and theName
    if(dimension == 3){
        str2Dyall = theOrder.str();
        twoDNum = intObNum;
        str2DName = theName.str();
        twoDYLen = length+1;
    }

    length = length+1;
    int countDraw = 0;

    if(dimension != 1 && dimension !=3){
        string linePoint;
        vector<string> getLinePoint;
        while(getline(theOrder,linePoint,';')){
            int sStr,eStr;
            getLinePoint.push_back(linePoint);
            stringstream sstrGetLinePoint;
            sstrGetLinePoint<<linePoint;
            string everyLine;
            vector<string> getEvery;
            int count = 1;

            while(getline(sstrGetLinePoint,everyLine,':')){
                if(count==1){
                    getEvery.push_back(everyLine);
                    sStr = stoi(everyLine);
                    count =2;
                    //-----------get the 2d x start
                    if(dimension == 2){
                        twoDxS<<':'<<sStr;
                    }
                    //-----------get the 3d x start
                    if(dimension == 4 ){
                        threeDxS<<':'<<sStr;
                    }
                    //-----------get the 3d y start
                    if(dimension == 5 ){
                        threeDyS<<':'<<sStr;
                    }
                    //-----------get the 3d z start
                    if(dimension == 6 ){
                        threeDzS<<':'<<sStr;
                    }

                }else
                    if(count==2){
                        getEvery.push_back(everyLine);
                        eStr = stoi(everyLine);
                        //-----------get the 2d s end
                        if(dimension == 2){
                            int lens = eStr - sStr;
                            twoDxE<<':'<<lens;
                        }
                        //-----------get the 3d x end
                        if(dimension == 4 ){
                            threeDxE<<':'<<eStr;
                        }
                        //-----------get the 3d y end
                        if(dimension == 5 ){
                            threeDyE<<':'<<eStr;
                        }
                        //-----------get the 3d y end
                        if(dimension == 6 ){
                            threeDzE<<':'<<eStr;
                        }
                    }

            }
            //get 3d object number
            if(dimension ==4 ){
                int tranint = countDraw++;
                stringstream tran;
                tran<<tranint;
                blockNum = tran.str();
                countDraw++;
            }
        }
        mainTab->setCurrentIndex(1);
    }
    //clear init stringstream after use
    if(dimension == 2){
        str2DxS = twoDxS.str();
        str2DxE = twoDxE.str();
    }
    if(dimension == 4){
        threexs = threeDxS.str();
        threexe = threeDxE.str();
        blockName = theName.str();
        threeDXLen = length;
    }
    if(dimension == 5){
        threeys = threeDyS.str();
        threeye = threeDyE.str();
        threeDYLen = length;
    }
    if(dimension == 6){
        threezs = threeDzS.str();
        threeze = threeDzE.str();
        threeDZLen = length;
    }

}
}

void MainWindow::getLineValue(){// get value in the lineedit
    QTextStream cout(stdout);

    if(transTo == 1){// 1D line
        inputValue = textInput->toPlainText().toStdString();
        //getPosition(1);
        draw1d = true;
        draw2d = false;
        draw3d = false;
        getPosition(1);
        if(queueWrongText.size() ==0){
        surfaceWidegt->update();
        }
        else{
            draw1d = false;
            mainTab->setCurrentIndex(0);
            int getQSize=queueWrongText.size();
            //std::cout<<"size of text:"<<getQSize<<" and text:"<<queueWrongText.front()<<std::endl;
            string strshow;
            for(int i = 0;i<getQSize;i++){
                //inhere show 1d wrong text
                strshow = strshow+queueWrongText.front() + "\n";
                queueWrongText.pop();
            }
            systemShow->setText(QString::fromStdString(strshow));
            clearQueue(queueWrongText);
            //QMessageBox::information(NULL,"Error","the input have some ambiguity please  see the detail in welcome widget");
        }

    }
    if(transTo == 2){// 2D rectengle A(o,o)B;
        string strLine = textInput->toPlainText().toStdString();
        const char* text = strLine.data();
        stringstream ssx,ssy,ssLine;
        string xrelation,yrelation;
        //std::cout<<" see "<<text<<std::endl;
        for(int c = 0; text[c]!='\0';c++){
            if( text[c] != '\n' && text[c] != ' '){
                ssLine<<text[c];
                if(text[c] == ';'){
                    //std::cout<<" in 2d every line "<<ssLine.str()<<std::endl;
                    const char* aryLine = ssLine.str().c_str();
                    int countPosi = 0;
                    for(int i = 0 ;i < strlen(aryLine);i++){

                        if(aryLine[i] == ','){
                            countPosi = 2;
                            ssy<<":";
                        }
                        if(aryLine[i] == '('){
                            countPosi = 1;
                            ssx<<":";
                        }
                        if(aryLine[i] == ')'){
                            countPosi = 0;
                            ssx<<":";
                            ssy<<":";

                        }
                        if(countPosi == 1 ){
                            if(aryLine[i] != '(' )
                                ssx<<aryLine[i];
                        }
                        if(countPosi ==2 ){
                            if(aryLine[i] != ',')
                                ssy<<aryLine[i];
                        }
                        if(aryLine[i] != '(' && aryLine[i] !=')' && aryLine[i] != ',' && countPosi == 0 ){
                            ssx<<aryLine[i];
                            ssy<<aryLine[i];
                        }
                    }
                    ssLine.clear();
                    ssLine.str("");
                }
            }
        }
        std::cout<<" x relation "<<ssx.str()<<" y relation "<<ssy.str()<<std::endl;
        xrelation = ssx.str();
        yrelation = ssy.str();

        bool haveWrong = false;
        inputValue = xrelation;
        getPosition(2);
        string strXWrong = "";
        if(queueWrongText.size() !=0){
            haveWrong = true;
            strXWrong = "in x axis: \n";
            int getQSize=queueWrongText.size();
            //std::cout<<"size of text:"<<getQSize<<" and text:"<<queueWrongText.front()<<std::endl;
            for(int i = 0;i<getQSize;i++){
                //inhere show 1d wrong text
                strXWrong = strXWrong+queueWrongText.front() + "\n";
                queueWrongText.pop();
            }
            clearQueue(queueWrongText);
        }

        inputValue = yrelation;
        getPosition(3);
        string strYWrong = "";
        if(queueWrongText.size() !=0){
            haveWrong = true;
            strYWrong = "in y axis : \n";
            int getQSize=queueWrongText.size();
            //std::cout<<"size of text:"<<getQSize<<" and text:"<<queueWrongText.front()<<std::endl;
            for(int i = 0;i<getQSize;i++){
                //inhere show 1d wrong text
                strYWrong = strYWrong+queueWrongText.front() + "\n";
                queueWrongText.pop();
            }
            clearQueue(queueWrongText);
        }

        draw2d = true;
        draw1d = false;
        draw3d = false;

        std::cout<<" get po 22"<<std::endl;
        if(!haveWrong){
        surfaceWidegt->update();
        //getPosition(3);
        }else{
            draw2d = false;
            draw1d = false;
            mainTab->setCurrentIndex(0);
            //std::cout<<"size of text:"<<getQSize<<" and text:"<<queueWrongText.front()<<std::endl;
            string strshow = strXWrong+strYWrong;
            systemShow->setText(QString::fromStdString(strshow));
            clearQueue(queueWrongText);
            //QMessageBox::information(NULL,"Error","the input have some ambiguity please  see the detail in welcome widget");
        }
    }
    if(transTo ==3){// 3D block
        string strLine = textInput->toPlainText().toStdString();
        const char* text = strLine.data();
        stringstream ssx,ssy,ssz,ssLine;
        string xrelation,yrelation,zrelation;
        //std::cout<<" see "<<text<<std::endl;
        for(int c = 0; text[c]!='\0';c++){
            if( text[c] != '\n' && text[c] != ' '){
                ssLine<<text[c];
                if(text[c] == ';'){
                    const char* aryLine = ssLine.str().c_str();
                    int countPosi = 0;
                    for(int i = 0 ;i < strlen(aryLine);i++){

                        if(aryLine[i] == ',' && countPosi ==2){
                            countPosi =3;
                            ssz<<":";
                        }
                        if(aryLine[i] == ',' && countPosi ==1){
                            countPosi = 2;
                            ssy<<":";
                        }
                        if(aryLine[i] == '('){
                            countPosi = 1;
                            ssx<<":";
                        }
                        if(aryLine[i] == ')'){
                            countPosi = 0;
                            ssx<<":";
                            ssy<<":";
                            ssz<<":";
                        }
                        if(countPosi == 1 ){
                            if(aryLine[i] != '(' )
                                ssx<<aryLine[i];
                        }
                        if(countPosi ==2 ){
                            if(aryLine[i] != ',')
                                ssy<<aryLine[i];
                        }
                        if(countPosi == 3){
                            if(aryLine[i] != ',')
                                ssz<<aryLine[i];
                        }
                        if(aryLine[i] != '(' && aryLine[i] !=')' && aryLine[i] != ',' && countPosi == 0 ){
                            ssx<<aryLine[i];
                            ssy<<aryLine[i];
                            ssz<<aryLine[i];
                        }
                    }
                    ssLine.clear();
                    ssLine.str("");
                }
            }
        }
        std::cout<<" x relation "<<ssx.str()<<" y relation "<<ssy.str()<<" z relation "<<ssz.str()<<std::endl;
        xrelation = ssx.str();
        yrelation = ssy.str();
        zrelation = ssz.str();

        //sort
        bool haveWrong = false;
        inputValue = xrelation;
        getPosition(4);
        string strXWrong = "";
        if(queueWrongText.size() !=0){
            haveWrong = true;
            strXWrong = "in x axis : \n";
            int getQSize=queueWrongText.size();
            for(int i = 0;i<getQSize;i++){
                //inhere show 1d wrong text
                strXWrong = strXWrong+queueWrongText.front() + "\n";
                queueWrongText.pop();
            }
            clearQueue(queueWrongText);
        }

        inputValue = yrelation;
        getPosition(5);
        string strYWrong = "";
        if(queueWrongText.size() !=0){
            haveWrong = true;
            strYWrong = "in y axis : \n";
            int getQSize=queueWrongText.size();
            for(int i = 0;i<getQSize;i++){
                //inhere show 1d wrong text
                strYWrong = strYWrong+queueWrongText.front() + "\n";
                queueWrongText.pop();
            }
            clearQueue(queueWrongText);
        }

        inputValue = zrelation;
        getPosition(6);
        string strZWrong = "";
        if(queueWrongText.size() !=0){
            haveWrong = true;
            strZWrong = "in z axis : \n";
            int getQSize=queueWrongText.size();
            for(int i = 0;i<getQSize;i++){
                //inhere show 1d wrong text
                strZWrong = strZWrong+queueWrongText.front() + "\n";
                queueWrongText.pop();
            }
            clearQueue(queueWrongText);
        }

        draw3d = true;
        draw2d = false;
        draw1d = false;

        if(!haveWrong){
        threeDWidget->update();
        mainTab->setCurrentIndex(2);
        }else{
            draw3d = false;
            std::cout<<" in 3 d block have wrong"<<std::endl;
            mainTab->setCurrentIndex(0);
            //std::cout<<"size of text:"<<getQSize<<" and text:"<<queueWrongText.front()<<std::endl;
            string strshow = strXWrong+strYWrong+strZWrong;
            systemShow->setText(QString::fromStdString(strshow));
            clearQueue(queueWrongText);
        }

    }


}

void MainWindow::clearQueue(queue<string>& q) {

    queue<string> empty;

    swap(empty, q);

}


void MainWindow::getFileInput(){

}

MainWindow::~MainWindow()
{

}
