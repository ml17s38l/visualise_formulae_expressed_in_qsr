#ifndef PAINTINGWIDGET_H
#define PAINTINGWIDGET_H
#include <QGLWidget>
#include <GL/glut.h>
#include <QKeyEvent>
#include <sstream>
#include <iostream>
#include <string>
#include <QMouseEvent>
#include "image.h"

using namespace std;

extern bool draw3d ;
extern std::string blockName;
extern std::string blockNum;
extern std::string threexs;
extern std::string threexe;
extern std::string threeys;
extern std::string threeye;
extern std::string threezs;
extern std::string threeze;
extern int threeDXLen;
extern int threeDYLen;
extern int threeDZLen;
class PaintingWidget: public QGLWidget
{
    Q_OBJECT
public:
    PaintingWidget(QWidget *parent);
public slots:
protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
    void keyPressEvent(QKeyEvent *event);

public:
    void cameraControl(int x);

private:
//    QOpenGLTexture* genTexture(int width, int height, const QString& text, int textPixelSize, const QColor &textColor);
    void blocktest(GLfloat x1, GLfloat x2,GLfloat y1,GLfloat y2, GLfloat z1,GLfloat z2);
    void backGround(GLfloat x1, GLfloat x2,GLfloat y1,GLfloat y2, GLfloat z1,GLfloat z2);

    void dealStr();
    void repa();
    void createimage(QString obName);
    void loadTexture(GLuint,image*);
    void drawAll();


    GLuint* textures_array = new GLuint[3];

    GLdouble eyeX = 0.0;//500.0
    GLdouble eyeY = 0.0;
    GLdouble eyeZ = 0.0;

    GLdouble viewX = eyeX;
    GLdouble viewY = eyeY;
    GLdouble viewZ = eyeZ;
    float dx = 0.0;
    float dy = 0.0;
    float dz = 0.0;

    float angleOfXZ = 0.0;
    float angleOfXZY = 0.0;

    float viewDistance = 100.0;//1000.0   100
    float degTORad = 3.14/180;
    int speed = 10.0;


};

#endif // PAINTINGWIDGET_H
