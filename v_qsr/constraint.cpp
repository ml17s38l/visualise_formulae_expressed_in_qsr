#include "constraint.h"
#include <sstream>
#include <iostream>
#include <fstream>
#include <string>
#include <map>

using namespace std;

//typedef struct mAp2{
//    std::string a;
//    std::string b;
//    std::array c;
//}nn;

Constrain::Constrain()
{
    // QTextStream cout(stdout);

}

void Constrain::initConstrain(){
    getFile();
    getConvert();
}

void Constrain::Allen13(){

}

void Constrain::getFile(){
    std::string fileName = "calculus/allen/composation.txt" ;

    std::ifstream fstr( fileName.c_str() )   ;   // open file and create data flow
    std::string lineStr ;

    std::stringstream sstr ;

    int i = 0 ;
    std::string  tranAry[3];
    std::string storeRelation;
    int count =0;

    while( std::getline(fstr, lineStr ) )    // read one line
    {
        if(i<13){
            const char* a = lineStr.c_str();
            for(int c = 0;a[c]!='\0';c++){
                if(a[c] != ':') {//according ：to split
                    //printf("%c",a[c]);
                    sstr<<a[c];
                    //std::string nn = sstr.str();
                    //std::cout<<nn<<std::endl;
                }

                if(a[c] == ':' || a[c +1] == '\0')
                {//character before : save in the tranAry[0-1-3]
                    std::string nn = sstr.str();
                    tranAry[count] = nn;
                    count++;
                    //std::cout<<nn<<std::endl;
                    sstr.clear() ;
                    sstr.str("");//clear catch
                }

            }
            count = 0;
            //map string<2 relation> string possible
            storeRelation = tranAry[0]+' '+tranAry[1];
            mapAllen[storeRelation] = tranAry[2];//relation1 relation= string1 possible= string2 use space to split
        }

        i++ ;
        sstr.clear() ;
        sstr.str("");//clear catch
    }

//    map<string,string>::iterator find;
//    find = mapAllen.find("< >");
//    if(find != mapAllen.end())
//        cout<<"find < > "<<find->second<<endl;
//    else
//        cout<<"do not find"<<endl;


    fstr.close() ; //close data flow

}


void Constrain::getConvert(){
    std::string fileName = "calculus/allen/convert.txt" ;

    std::ifstream fstr( fileName.c_str() )   ;   // open file，create data flow
    std::string lineStr ;

    std::stringstream sstr ;

    int i = 0 ;
    std::string  tranAry[2];
    int count =0;

    while( std::getline(fstr, lineStr ) )    // read one line
    {
        if(i<13){
            const char* a = lineStr.c_str();
            for(int c = 0;a[c]!='\0';c++){
                if(a[c] != ':') {//according：to split
                    //printf("%c",a[c]);
                    sstr<<a[c];
                    //std::string nn = sstr.str();
                    //std::cout<<nn<<std::endl;
                }

                if(a[c] == ':' || a[c +1] == '\0')
                {//character before : save in the tranAry[0-1-3]
                    std::string nn = sstr.str();
                    tranAry[count] = nn;
                    count++;
                    //std::cout<<nn<<std::endl;
                    sstr.clear() ;
                    sstr.str("");//clear cache
                }

            }
            count = 0;
            mapConvertAllen[tranAry[0]]=tranAry[1];
        }

        i++ ;
        sstr.clear() ;
        sstr.str("");//clear cache
    }

//    map<string,string>::iterator find;
//    find = mapConvertAllen.find("<");
//    if(find != mapConvertAllen.end())
//        cout<<"find <"<<find->second<<endl;
//    else
//        cout<<"do not find convert"<<endl;


    fstr.close() ; //close data flow

}

