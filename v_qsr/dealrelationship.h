#ifndef DEALRELATIONSHIP_H
#define DEALRELATIONSHIP_H
#include <QString>
#include "constraint.h"
#include <map>
#include "ordergraph.h"
#include <sstream>
#include <QtGui>

using namespace std;

extern std::string inputValue;
extern queue<string> queueWrongText;
class dealRelationship
{
public:
    dealRelationship();
public:
    int getSame(std::string sameStr);

    void initDeal(stringstream *a,stringstream*b,int *maxLength,int *objNum);
    void initOrder(stringstream *a, stringstream*b, int *maxLength,int *objNum);
    void orderObject();
    void testSame(std::string obA, std::string obB);
    void reviseSame(std::string onA,std::string obB);
    bool findCir(string re);

    std::map<std::string,std::string> mapObjectBit;
    std::map<std::string,std::string> mapObjectRel;
    std::map<std::string,std::string> mapDealSame;
    std::map<std::string,std::string> mapFindCir;

    stringstream returnNameToDraw(stringstream);
    stringstream returnPointToDraw(stringstream);
};

#endif // DEALRELATIONSHIP_H
