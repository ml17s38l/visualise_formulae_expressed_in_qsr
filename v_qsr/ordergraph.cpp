#include "ordergraph.h"
#include <iostream>
#include <sstream>
#include <queue>
using namespace std;

ordergraph::ordergraph()
{

}


void ordergraph::initGraph(graph *&iGraph, int vertexNum)
{
    if(iGraph == NULL){
        iGraph = new graph;
        iGraph->vertexList = new vertexNode[vertexNum];
        iGraph->vertexes = vertexNum;
        iGraph->edges = 0;
        for(int i =0; i<vertexNum;i++){
            //stringstream s;
            //s<<"v"<<i+1;
            //s<<i+1;
            //s>>iGraph->vertexList[i].vertexLabel;
            iGraph->vertexList[i].vertexLabel = i +1;
            //iGraph->vertexList[i].visited = false;
            iGraph->vertexList[i].indegree = 0;
            iGraph->vertexList[i].first = NULL;
            iGraph->vertexList[i].flag = -1;
            iGraph->vertexList[i].firstR = NULL;
        }

    }
}

void ordergraph::emptyGraph(graph *&eGraph){
    if(eGraph == NULL)
        return;
    for(int i = 0; i<eGraph->vertexes;i++){
        edgeNode *pEdge = eGraph->vertexList[i].first;
        while(pEdge != NULL){
            edgeNode *tmp = pEdge;
            pEdge = pEdge->next;
            delete tmp;
        }
    }
    delete eGraph;
}

void ordergraph::addEdge(graph *&aGraph, int vOut, int vIn,string reText){
    //some wrong case
    if (aGraph == NULL)
        return;
    if (vOut < 0 || vOut > aGraph->vertexes -1)return;
    if (vIn < 0 || vIn > aGraph->vertexes - 1)return;
    if (vOut == vIn){
        std::cout<<"worng input like a<a"<<std::endl;
        return;
    }//loop add prompt like: a<a

    edgeNode *p = aGraph->vertexList[vOut].first;
    relation *pText = aGraph->vertexList[vOut].firstR;
    //current vertex do not have relation
    if(p==NULL){
        aGraph->vertexList[vOut].first = new edgeNode;
        aGraph->vertexList[vOut].first->next = NULL;
        aGraph->vertexList[vOut].first->vtxNo = vIn;
        aGraph->edges++;
        aGraph->vertexList[vIn].indegree++;
        aGraph->vertexList[vOut].firstR = new relation;
        aGraph->vertexList[vOut].firstR->next = NULL;
        aGraph->vertexList[vOut].firstR->text = reText;
        return;
    }

    //loop let p(node) in the last
    while(p->next != NULL){
        //if exits a->b
        if(p->vtxNo == vIn){
            string old = pText->text;
            pText->text = old+";"+reText;
            return;
        }
        p = p->next;
    }

    //loop let pText in the last
    while(pText->next != NULL){
        pText = pText->next;
    }

    //the last one exits a->b
    if(p->vtxNo == vIn)
    {
        string old = pText->text;
        pText->text = old+";"+reText;
        return;
        }

    //add new node in the last
    edgeNode *node = new edgeNode;
    relation *nodeText = new relation;
    node->next = NULL;
    node->vtxNo = vIn;
    p->next = node;
    //text
    nodeText->next = NULL;
    nodeText->text = reText;
    pText->next = nodeText;

    aGraph->edges++;
    aGraph->vertexList[vIn].indegree++;
}

int ordergraph::countIndegree(graph *iGraph, int vtxNum){
    //wrong case
    if(iGraph == NULL) return -1;
    if(vtxNum < 0 || vtxNum > iGraph->vertexes -1) return -2;

    int degree = 0;
    for(int i = 0; i<iGraph->vertexes;i++){
        edgeNode *p = iGraph->vertexList[i].first;
        while(p != NULL){
            if(p->vtxNo == vtxNum)
            {
                degree ++;
                break;
            }
            p=p->next;
        }
    }
    return degree;
}

stringstream ordergraph::topologicalSort(graph *tGraph){
    stringstream orderSt;
    //the graph is null have wrong--------**********need add in wrong message delete the return information**********************
    if(tGraph == NULL){
        orderSt.clear();
        orderSt.str("");
        orderSt<<"error";
        return orderSt;
    }
    int counter = 0;
    queue <int> qVex;
    queue <int> qVexSave;
    for(int i = 0; i <tGraph->vertexes;i++){
        //the graph is null have wrong--------**********need add in wrong message delete the return information**********************
        if(countIndegree(tGraph,i)==-1 || countIndegree(tGraph,i) == -2){
            orderSt.clear();
            orderSt.str("");
            orderSt<<"error";
            return orderSt;
        }
        if(countIndegree(tGraph,i)==0){
            qVex.push(i);
            tGraph->vertexList[i].flag =0;
        }
    }
    while(!qVex.empty()){
        int vtxNo = qVex.front();
        counter++;
        orderSt<<tGraph->vertexList[vtxNo].vertexLabel;
        //cout<<tGraph->vertexList[vtxNo].vertexLabel;
        if(counter != tGraph->vertexes)
            orderSt<<">";
        //cout<<">";
        qVexSave.push(vtxNo);
        qVex.pop();

        edgeNode *p = tGraph->vertexList[vtxNo].first;
        while(p != NULL)
        {
            int vtxN = p->vtxNo;
            p = p->next;

            if(--tGraph->vertexList[vtxN].indegree == 0){
                qVex.push(vtxN);
                tGraph->vertexList[vtxN].flag =0;

            }
        }
    }

    //have ring
    if(counter != tGraph->vertexes){
        ifRing = true;
        //find all the circle
        //std::cout<<" have a crial"<<std::endl;
        for(int i = 0;i<tGraph->vertexes;i++){
            if (tGraph->vertexList[i].flag == -1){
                if(saveFirt.size() > 0){
                    saveFirt.pop();

                }
                saveFirt.push(i);
            loopRing(tGraph,i);
            ringText.push("and ");
            //if this do not find a circle delete and
            if(findRing == false){
                ringText.pop();
            }
            }
            //continue to find circle
            findRing = false;
        }

        int getsize = ring.size();
        for(int i = 0;i<getsize;i++){
            std::cout<<" the ring is "<<ring.top()<<std::endl;
            ring.pop();
        }

    }
    std::cout<<orderSt.str()<<" and the use num:"<<counter<<std::endl;
    return orderSt;
}


void ordergraph::loopRing(graph *lGraph, int node){

    lGraph->vertexList[node].flag = 1;
    ring.push(node);
    edgeNode *check = lGraph->vertexList[node].first;
    relation *checkText = lGraph->vertexList[node].firstR;
    while (check != NULL) {
        if(lGraph->vertexList[check->vtxNo].flag == 1 && check->vtxNo == saveFirt.front()){
            ringText.push(checkText->text);
            findRing = true;
            return;
        }
        check = check->next;
        checkText = checkText->next;
    }

    edgeNode *p = lGraph->vertexList[node].first;
    relation *pT = lGraph->vertexList[node].firstR;
    while(p != NULL){
        if(lGraph->vertexList[p->vtxNo].flag == -1){
            ringText.push(pT->text);
            loopRing(lGraph,p->vtxNo);
        }
        p = p->next;
        pT = pT->next;
    }

    if(findRing == false){
        ring.pop();
        ringText.pop();
    }

}


//void ordergraph::clear(queue<int>& q) {

//    queue<int> empty;

//    swap(empty, q);

//}
